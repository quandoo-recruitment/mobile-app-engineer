# Flutter Engineer Coding Challenge

## Introduction
Thank you for applying for the Flutter Engineer position at Quandoo. In order to have a good understanding of your skill level, we would like to proceed with a technical assessment.

## Task description
It's a really common standard that a consumer facing application fetch its data through a REST API.
Let's build a merchant browser, which fetches data from Quandoo's test public API.
The app should consist of a simple navigation-based restaurant browser and restaurant detail view.
Please access our documentation to get to know the endpoints and response format
https://docs.quandoo.com/interactive-api/ 

### Description
On launch your app should fetch the first 120 merchants.
When the user taps on the merchant, it should lead to a merchant detail page.

### Required
Build a simple tablet Flutter app that provides information about restaurants.
  - Your cell must show the first restaurant image and name only
  - Your detail page must show: restaurant images, name, rating, address and any other information that you might find useful.
  - Ensure that your scroll experience is smooth and does not show messy results.
  - **Show us what you got**: Focus on a **good architecture**, **separation of concerns** and **test coverage**. 
  - A good and comprehensive readme file is also welcome.
  - The usage of third parties is permitted and will be taken into account while reviewing your code.
  - Make sure your project compiles and it is properly setup. 

### Submitting your solution
  - Fork it to a **[!]**private**[!]** gitlab repository (go to `Settings -> General -> Visibility, project features, permissions -> Project visibility`).
  - Share the project with gitlab user *quandoo_recruitment_task* (go to `Settings -> Members -> Invite member`, find the user in `Select members to invite` and set `Choose a role permission` to `Developer`)
  - Send us an **ssh** clone link to the repository.

### Tips
  - Design is not a priority but we are going to definitely look how you define your widgets and feed them with data.
  - merchants = restaurants.
  - Although this is a Flutter Engineer role, if you think you will be able to demonstrate better your skills writing native Android or iOS and is willing to work with Flutter, feel free to implement it with the framework you feel more comfortable with.
  
[Give us your feedback about this task.](https://docs.google.com/forms/d/1QfYEJRDgtGOtdP3Tejrrr7wezGLHuejBgo6bauGSdl0/viewform?edit_requested=true)
